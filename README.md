 Домашнее задание 3
Добавлено: 29.12.2019 22:54
Задание 3

On GitLab:

1) Create new project name: java-practice-02-git; Visibility level: private; Intialize with README: false

2) Setup SSH key to local ${user.home}/.ssh directory (let it be ED25519 without passphrase)

3) Add public SSH to GitLab

4) Invite access to your repository for my account home.igor@gmail.com (as Developer)

Working with Git bash:

5) Clone project to your workstation using git-protocol with ssh

6) Check the global user settings, if something is wrong - redefine them

7) Stay in master (local workcopy)

8) Create file init.empty and commit it to current branch with message 'init empty'

9) Create new branches master_rebase, master_merge, primary, secondary (all from master)

10) Checkout to primary

11) Create a .gitignore file and add any folder/file masks that you want to exclude (in fact, this is not a maven or gradle project)

12) Create a README.md file with some body (here and for similar steps you can choose any text editor)

13) Create at least one file and one folder that migth be ignored by mask

14) Commit all changes (one commit per each file, your commit message should have a concise and clear meaning, be careful with ignored files)

15) Checkout to secondary

16) Find .gitignore commit from primary branch and apply it to secondary

17) Create another file here and commit it too

18) Merge primary into master_merge

19) Merge secondary into master_merge

20) Rebase secondary onto master_rebase

21) Rebase primary onto master_rebase (with squash commits - let it be the last one)

22) Resolve all conflicts if necessary*

23) Checkout to master_merge

24) And create new brach tertiary from it

25) Checkout to tertiary

26) Delete last 2 commits (use --hard option)

27) Find and revert commit with .gitignore file

28) Then apply command 'git add .' and commit it

29) Go back to master_merge

30) And rename it to develop

31) Push all local braches to remote 

32) Do 'git reflog show --all' command and save output to git-reflog.txt and attach it to your LMS-sys homework

On GitLab:

26) Create MR (merge request) from develop branch to master, me as assignee, delete source branch after merge option: false